#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define nil NULL
#define info(P) (P)->info
#define nextA(P) (P)->nextA
#define nextO(P) (P)->nextO
#define first(L) (L).first
#define head(L) (L).head
#define nMax 10

typedef struct{
	float T[nMax+1];
	int neff;
}Tabel;

typedef struct tElmNilai *addressN;

typedef struct tElmNilai{
	float info;
	addressN nextA;
}ElmNilai;

typedef struct{
	addressN head;
}ListNilai;

typedef struct{
	int No;
	char Nama[30];
	char Nim[3];
	float c1;
	float c2;
}Data;

typedef struct tElmMh *addressMh;

typedef struct tElmMh{
	Data info;
	addressN nextA;
	addressMh nextO;
}ElmMh;

typedef struct{
	addressMh first;
}ListMh;

//Prototype
void CreateListMh(ListMh *Lm);
void CreateListN(ListNilai *Ln);
void CreateTab(Tabel *T);
void AddListMh(ListMh *Lm, Data D, ListNilai Ln);
void AddListNilai(ListNilai *Ln, float x);
void PreCetakList(ListMh Lm);
addressMh AlokasiMh(Data D);
addressN AlokasiNilai(float x);
void Pusat(ListMh *Lm, int x, int y);
void CetakTab (Tabel T);
void AddTabNilai(Tabel *T, float x);
void CetakList(ListMh Lm);
Tabel GetKel(ListMh Lm, int x);
void UbahJarak(ListMh *Lm, Tabel c1, Tabel c2);
//void PusatIterasi(ListMh Lm,)

int main(){
	//Kamus
	float x;
	ListMh MyList;
	Data MD, D;
	ListNilai MyNilai;
	Tabel c1, c2;
	FILE *in=fopen("Data.txt","r");
	int i, k1, k2, y;
	
	//Algoritma
	CreateListMh(&MyList);
	if(!in){
		printf("Tidak Ada File");
	}else{
		while(!feof(in)){
			fscanf(in,"%d#", &D.No);fflush(stdin);
			fscanf(in,"%[^#]#", &D.Nim);fflush(stdin);
			fscanf(in,"%[^#]#", &D.Nama);fflush(stdin);
			fscanf(in,"%d#", &D.c1);fflush(stdin);
			fscanf(in,"%d#", &D.c2);fflush(stdin);
			CreateListN(&MyNilai);
			for(i=1;i<=3;i++){
				fscanf(in,"%f#", &x);fflush(stdin);
				AddListNilai(&MyNilai,x);
			}
			AddListMh(&MyList,D,MyNilai);
			fscanf(in,"\n");fflush(stdin);
		}
		fclose(in);
	}
	
	PreCetakList(MyList);
	
	printf("Input K1 = "), scanf("%d",&k1);
	printf("Input K2 = "), scanf("%d",&k2);
	
	Pusat(&MyList,k1,k2);
	
	CetakList(MyList);
	
	printf("Input Iterasi = "), scanf("%d",&y);
	for(i=1;i<=y;i++){
		c1=GetKel(MyList,1);
		c2=GetKel(MyList,2);
		CetakList(MyList);
		UbahJarak(&MyList,c1,c2);	
	}
	
	
	return 0;
}

//Body of Prototype
void CreateListMh(ListMh *Lm){
	first(*Lm)=nil;
}

void CreateListN(ListNilai *Ln){
	head(*Ln)=nil;
}

void CreateTab(Tabel *T){
	(*T).neff=0;
}

void AddListMh(ListMh *Lm, Data D, ListNilai Ln){
	//Kamus
	addressMh Ao, P;
	
	//Algoritma
	Ao=first(*Lm);
	P=AlokasiMh(D);
	nextA(P)=head(Ln);
	if(Ao==nil){
		first(*Lm)=P;
	}else{
		while(nextO(Ao)!=nil){
			Ao=nextO(Ao);
		}
		nextO(Ao)=P;
	}
}

void AddListNilai(ListNilai *Ln, float x){
	//Kamus
	addressN Aa, P;
	
	//Algoritma
	Aa=head(*Ln);
	P=AlokasiNilai(x);
	if(Aa==nil){
		head(*Ln)=P;
	}else{
		while(nextA(Aa)!=nil){
			Aa=nextA(Aa);
		}
		nextA(Aa)=P;
	}
}

void PreCetakList(ListMh Lm){
	//Kamus
	addressMh Ao;
	addressN Aa;
	Data D;
	int i;
	
	//Algoritma
	Ao=first(Lm);
	i=0;
	printf("==================================================================================\n");
	printf("No\tNIM\tNama\t\tNilai 1\tNilai 2\tNilai 3\n");
	printf("==================================================================================\n");
	while(Ao!=nil){
		i++;
		D=info(Ao);
		printf("%d\t", D.No);
		printf("%s\t",D.Nim);
		printf("%s\t",D.Nama);
		Aa=nextA(Ao);
		while(Aa!=nil){
			printf("%.0f\t",info(Aa));
			Aa=nextA(Aa);
		}
		printf("\n");
		Ao=nextO(Ao);
	}
	printf("==================================================================================\n");
}

void CetakList(ListMh Lm){
	//Kamus
	addressMh Ao;
	addressN Aa;
	Data D;
	
	//Algoritma
	Ao=first(Lm);
	printf("==================================================================================\n");
	printf("No\tNIM\tNama\t\tNilai 1\tNilai 2\tNilai 3\tc1\tc2\tKel\n");
	printf("==================================================================================\n");
	while(Ao!=nil){
		D=info(Ao);
		printf("%d\t", D.No);
		printf("%s\t",D.Nim);
		printf("%s\t",D.Nama);
		Aa=nextA(Ao);
		while(Aa!=nil){
			printf("%.0f\t",info(Aa));
			Aa=nextA(Aa);
		}
		printf("%.2f\t",D.c1);
		printf("%.2f\t",D.c2);
		if(D.c1<D.c2){
			printf("1");
		}else{
			printf("2");
		}
		printf("\n");
		Ao=nextO(Ao);
	}
	printf("==================================================================================\n");
}

addressMh AlokasiMh(Data D){
	//Kamus
	addressMh Px;
	
	//Algoritma
	Px=(addressMh)malloc(sizeof(ElmMh));
	info(Px)=D;
	nextA(Px)=nil;
	nextO(Px)=nil;
	
	return Px;
}

addressN AlokasiNilai(float x){
	//Kamus
	addressN Px;
	
	//Algoritma
	Px=(addressN)malloc(sizeof(ElmNilai));
	info(Px)=x;
	nextA(Px)=nil;
	
	return Px;
}

void Pusat(ListMh *Lm, int x, int y){
	//Kamus
	Tabel satu, dua, c1, c2;
	addressMh Ao;
	addressN Aa;
	Data D;
	float tmp1, tmp2;
	int i;
	
	//Algoritma
	Ao=first(*Lm);
	while(Ao!=nil){
		D=info(Ao);
		if(D.No==x){
			CreateTab(&satu);
			Aa=nextA(Ao);
			while(Aa!=nil){
				AddTabNilai(&satu,info(Aa));
				Aa=nextA(Aa);
			}
		}else if(D.No==y){
			CreateTab(&dua);
			Aa=nextA(Ao);
			while(Aa!=nil){
				AddTabNilai(&dua,info(Aa));
				Aa=nextA(Aa);
			}
		}
		Ao=nextO(Ao);
	}
	
	Ao=first(*Lm);
	while(Ao!=nil){
		D=info(Ao);
		i=0;
		Aa=nextA(Ao);
		CreateTab(&c1);
		CreateTab(&c2);
		while(Aa!=nil){
			i++;
			tmp1=info(Aa)-satu.T[i];
			tmp2=info(Aa)-dua.T[i];
			tmp1=pow(tmp1,2);
			tmp2=pow(tmp2,2);
			AddTabNilai(&c1,tmp1);
			AddTabNilai(&c2,tmp2);
			Aa=nextA(Aa);
		}
		tmp1=0;
		tmp2=0;
		for(i=1;i<=3;i++){
			tmp1=tmp1+c1.T[i];
			tmp2=tmp2+c2.T[i];
		}
		tmp1=sqrt(tmp1);
		tmp2=sqrt(tmp2);
		D.c1=tmp1;
		D.c2=tmp2;
		info(Ao)=D;
		
		Ao=nextO(Ao);
	}
	
}

void CetakTab (Tabel T){
	int i;

	for(i=1; i<=3; i++){
		printf("%.0f",T.T[i]);
	}
	printf("\n");
}

void AddTabNilai(Tabel *T, float x){
	if((*T).neff<nMax){
		(*T).neff++;
		(*T).T[(*T).neff]=x;
	}
}

Tabel GetKel(ListMh Lm, int x){
	//Kamus
	addressMh Ao;
	addressN Aa;
	Tabel T;
	Data D;
	float i;
	
	//Algoritma
	Ao=first(Lm);
	CreateTab(&T);
	i=0;
	if(x==1){
		while(Ao!=nil){
			D=info(Ao);
			i++;
			if(D.c1<D.c2){
				AddTabNilai(&T,i);
			}
			Ao=nextO(Ao);
		}
	}else{
		while(Ao!=nil){
			D=info(Ao);
			i++;
			if(D.c1>D.c2){
				AddTabNilai(&T,i);
			}
			Ao=nextO(Ao);
		}
	}
	return T;
}

void UbahJarak(ListMh *Lm, Tabel c1, Tabel c2){
	//Kamus
	addressMh Ao;
	addressN Aa;
	int i, j, x;
	Data D;
	Tabel p1, p2, satu, dua;
	float k, tmp1, tmp2;
	
	//Algoritma
	Ao=first(*Lm);
	k=0;
	tmp1=0;
	tmp2=0;
	p1.neff=3;
	p2.neff=3;
	for(i=1;i<=3;i++){
		p1.T[i]=0;
		p2.T[i]=0;
	}
	while(Ao!=nil){
		k++;
		D=info(Ao);
		Aa=nextA(Ao);
		
		for(i=1;i<=c1.neff;i++){
			if(c1.T[i]==k){
				j=0;
				while(Aa!=nil){
					j++;
					p1.T[j]=p1.T[j]+info(Aa);
					Aa=nextA(Aa);
				}
				tmp1++;
			}
		}
		
		for(i=1;i<=c2.neff;i++){
			if(c2.T[i]==k){
				j=0;
				while(Aa!=nil){
					j++;
					p2.T[j]=p2.T[j]+info(Aa);
					Aa=nextA(Aa);
				}
				tmp2++;
			}
		}
		
		Ao=nextO(Ao);
	}
	
	
	for(i=1;i<=p1.neff;i++){
		p1.T[i]=p1.T[i]/tmp1;
		printf("%.0f  ",p1.T[i]);
	}
	
	printf("Pusat 1 \n");
	
	for(i=1;i<=p2.neff;i++){
		p2.T[i]=p2.T[i]/tmp2;
		printf("%.0f  ",p2.T[i]);
	}
	
	printf("Pusat 2 \n");
	
	//
	
	tmp1=0;
	tmp2=0;
	Ao=first(*Lm);
	while(Ao!=nil){
		D=info(Ao);
		i=0;
		Aa=nextA(Ao);
		CreateTab(&satu);
		CreateTab(&dua);
		while(Aa!=nil){
			i++;
			tmp1=info(Aa)-p1.T[i];
			tmp2=info(Aa)-p2.T[i];
			tmp1=pow(tmp1,2);
			tmp2=pow(tmp2,2);
			AddTabNilai(&satu,tmp1);
			AddTabNilai(&dua,tmp2);
			Aa=nextA(Aa);
		}
		tmp1=0;
		tmp2=0;
		for(i=1;i<=3;i++){
			tmp1=tmp1+satu.T[i];
			tmp2=tmp2+dua.T[i];
		}
		tmp1=sqrt(tmp1);
		tmp2=sqrt(tmp2);
		D.c1=tmp1;
		D.c2=tmp2;
		info(Ao)=D;
		
		Ao=nextO(Ao);
	}
}
